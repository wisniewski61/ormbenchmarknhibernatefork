﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using OrmBenchmark.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OrmBenchmark.NHibernate
{
    public class NHibernateExecuter : IOrmExecuter
    {
        private ISession session;

        public string Name
        {
            get
            {
                return "NHibernate";
            }
        }

        public void Finish()
        {
            session.Close();
        }

        public IList<dynamic> GetAllItemsAsDynamic()
        {
            return null;
        }

        public IList<IPost> GetAllItemsAsObject()
        {
            return session.CreateCriteria<Posts>().List<IPost>();
        }

        public dynamic GetItemAsDynamic(int Id)
        {
            return null;
        }

        public IPost GetItemAsObject(int Id)
        {
            return session.Get<Posts>(Id) as IPost;
        }

        public void Init(string connectionString)
        {
            var cfg = new Configuration();

            cfg.DataBaseIntegration(x => {
                x.ConnectionString = connectionString;
                x.Driver<SqlClientDriver>();
                x.Dialect<MsSql2012Dialect>();
            });

            cfg.AddAssembly(Assembly.GetExecutingAssembly());

            session = cfg.BuildSessionFactory().OpenSession();
        }
    }
}
