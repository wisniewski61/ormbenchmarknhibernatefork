﻿using OrmBenchmark.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrmBenchmark.NHibernate
{
    public class Posts : IPost
    {
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime LastChangeDate { get; set; }
        public virtual int? Counter1 { get; set; }
        public virtual int? Counter2 { get; set; }
        public virtual int? Counter3 { get; set; }
        public virtual int? Counter4 { get; set; }
        public virtual int? Counter5 { get; set; }
        public virtual int? Counter6 { get; set; }
        public virtual int? Counter7 { get; set; }
        public virtual int? Counter8 { get; set; }
        public virtual int? Counter9 { get; set; }
    }
}
